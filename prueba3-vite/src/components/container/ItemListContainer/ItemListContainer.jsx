import { useState, useEffect } from 'react'

import { collection, doc, getDoc, getDocs, getFirestore, limit, orderBy, query, where } from 'firebase/firestore'
import { Link, useParams } from 'react-router-dom'
import { gFetch } from '../../../helpers/getFetch'
import ItemList from '../../ItemList/ItemList'

const Loading = () => {
    // EStados
    // useEffect(() => {
    //     console.log('loading')
    //     return () => console.log('desmontando loading')
    // })

    return (
        <div>
            <h1>Loading...</h1>
            {/* componentes hijos */}
        </div>
    )
}

const ItemListContainer = ({ greeting }) => {
    const [products, setProducts] = useState([])
    const [product, setProduct] = useState({})
    const [loading, setLoading] = useState(true)
    const [bool, setBool] = useState(true)
    const { categoriaId } = useParams()
    
    console.log(products)

    useEffect(() => {
        const db = getFirestore()
        if (categoriaId) {
            const queryCollection = collection(db, 'items')
            const queryCollectionFilter = query(queryCollection, where('categoria', '==', categoriaId))
            getDocs(queryCollectionFilter)
            .then(resp => setProducts( resp.docs.map(prod => ( { id: prod.id, ...prod.data() } ) ) ) )
            .catch( err => console.log(err) )
            .finally(()=> setLoading(false))             
        } else {           
            const queryCollection = collection(db, 'items')
            getDocs(queryCollection)
            .then(resp => setProducts( resp.docs.map(prod => ( { id: prod.id, ...prod.data() } ) ) ) )
            .catch( err => console.log(err) )
            .finally(()=> setLoading(false))         
        }
    }, [ categoriaId ])
   
   
        
    //ejemplo de evento
    const handleClick=(e)=>{
        e.preventDefault() 
        setBool(!bool)
    }

    const handleAgregar=()=>{
        setProducts([
            ...products,
            { id: products.length + 1 , name: "Gorra 7", url: 'https://www.remerasya.com/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/e/remera_negra_lisa.jpg', categoria: "remera" , price: 2 }
        ])
    }
    
    
        console.log('itemListContainer')
    return (
        <div>
            <button onClick={handleClick}>Cambiar estado </button>
            <button onClick={handleAgregar}>Agregar Item </button>
           { loading 
                ?  <Loading />
                :  <ItemList products={products} />                
            }
        </div>
    )
}

export default ItemListContainer



{/* <div            
    className='col-md-4 p-1'                                                           
>                    
    <div className="card w-100 mt-5" >
        <div className="card-header">
            {`${prod.name} - ${prod.categoria}`}
        </div>
        <div className="card-body">
            <img src={prod.foto} alt='' className='w-50' />
            {prod.stock}                                                            
        </div>
        <div className="card-footer"> 
            <Link to={`/detalle/${prod.id}`} >
                <button className="btn btn-outline-primary btn-block">
                    detalle del producto
                </button>   
            </Link> 
        </div>
    </div>                                                                                                                            
</div> */}