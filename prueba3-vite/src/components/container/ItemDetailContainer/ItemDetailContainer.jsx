import { doc, getDoc, getFirestore } from "firebase/firestore";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { TextComponent, TextComponent2, TextComponent3, TextComponent4, TextComponent5, TextComponent6, TextComponent7 } from "../../../../clases/clase11/ComponenteEjemplosCondicionales";
import { gFetch } from "../../../helpers/getFetch";
import ItemDetail from "../../ItemDetail/ItemDetail";


const ItemDetailContainer = () => {
    const [ product, setProduct ] = useState({});
    const [ loading, setLoading ] = useState(true);
    const { id } = useParams();

    // products.filter((product) => product.id === id) => []
    // products.find((product) => product.id === id) => {}

    //detalle 
    useEffect(() => {
        const db = getFirestore() 
        const queryProduct = doc(db, 'items', id )
        getDoc(queryProduct)
        .then(resp => setProduct( { id: resp.id, ...resp.data() } ))
        .catch( err => console.log(err) )
        .finally(()=> setLoading(false))
    }, [id])

    // useEffect(()=>{
    //     gFetch(id)
    //     .then(resp => setProduct(resp))
    //     .catch( err => console.log(err))
    //     .finally(() => setLoading(false))
    // })
    return (        
        <div>
            {/* <TextComponent >
            </TextComponent> */}
            { loading 
                ? <h2>Cargando...</h2> 
                : <ItemDetail product={product} /> 
            }
              
                
        </div>
    )
}

export default ItemDetailContainer