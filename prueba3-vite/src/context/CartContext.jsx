import {useState, createContext, useContext} from 'react'

const CartContext = createContext()
export const useCartContext = () => useContext(CartContext)

const CartContextProvider = ({children}) => {
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cartList')))

    const guardarLocalStorage = (clave, valor) => {
        localStorage.setItem(clave,JSON.stringify(valor))    
    }

    // función que agrega un producto al carrito
    const addProduct = (product) => {        
        // console.log(localStorage.getItem('cartList'))
        const indexProduct = cart.findIndex(prod => prod.id === product.id)
        if(indexProduct === -1 ){
            // localStorage.setItem('cartList',JSON.stringify([ ...cart, product]))
            guardarLocalStorage('cartList', [ ...cart, product]) 
            setCart([
                ...cart,
                product
            ])
        }else{
            const cantidadvieja = cart[indexProduct].cantidad
            cart[indexProduct].cantidad = cantidadvieja + product.cantidad
            setCart( [...cart] )
            // localStorage.setItem('cartList', JSON.stringify([...cart]))  
            guardarLocalStorage('cartList', [...cart])
        }        
        
    }

    // función total del precio
    const precioTotal = ()=>{
        return cart.reduce((acumPrecio, prodObj) => acumPrecio = acumPrecio + (prodObj.price * prodObj.cantidad) , 0) // <- precioTotal
    }

    const cantidadTotal = ()=>{
        return cart.reduce((contador, produObject) => contador += produObject.cantidad , 0) 
    }

    // función que elimina un producto del carrito
    const eliminarProducto = (id) => {
        setCart( cart.filter(prod => prod.id !== id ) )
        // localStorage.setItem('cartList', JSON.stringify(cart.filter(prod => prod.id !== id )))
        guardarLocalStorage('cartList', cart.filter(prod => prod.id !== id ))
    }
    
    // función que vaciar carrito
    const emptyCart = () => {
        setCart([])
        // localStorage.setItem('cartList', JSON.stringify([]))
        guardarLocalStorage('cartList', [])
    }


    // cantidades total de productos


    return (
        <CartContext.Provider value={{
            cart, 
            addProduct,
            emptyCart,
            precioTotal,
            cantidadTotal,
            eliminarProducto            
        }}>
            {children}
        </CartContext.Provider>
    )
}

export default CartContextProvider





















// const index = cartList.findIndex(product => product.id === item.id) // -1 sino está 
//         if (index !== -1) {
//             const cantidadVieja = cartList[index].cantidad
//             console.log(cantidadVieja)

//             cartList[index].cantidad = cantidadVieja + item.cantidad
//             console.log(cartList[index].cantidad)
            
//             setCartList( [ ...cartList ] )
//         } else {
//             setCartList( [
//                 ...cartList,
//                 item
//             ] )             
//         }    