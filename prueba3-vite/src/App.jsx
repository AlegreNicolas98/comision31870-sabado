import { useEffect, useState } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'

import NavBar from './components/NavBar/NavBar'
import ItemListContainer from './components/container/ItemListContainer/ItemListContainer';
import Cart from './components/container/Cart/Cart';
import ItemDetailContainer from './components/container/ItemDetailContainer/ItemDetailContainer';


import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import CartContextProvider from './context/CartContext';

 

// Name('fede') => <Name params='fede' />


function App() {

    return (
        <BrowserRouter>
            <CartContextProvider>
                <div>
                    <NavBar /> 
                    <Routes>
                        <Route index path='/' element={<ItemListContainer greeting={' Hola soy ItemListcontainer '} /> }  />
                        <Route path='/categoria/:categoriaId' element={<ItemListContainer greeting={' Hola soy ItemListcontainer '} /> }  />
                        <Route path='/detail/:id' element={ <ItemDetailContainer /> }  />
                        <Route path='/cart' element={ <Cart /> }  />                              
                        {/* <Route path='/404' element={ <404notfound /> }  />                               */}
                    
                        <Route path='*' element={ <Navigate to='/' /> } />
                    </Routes>
                </div>
            </CartContextProvider>
        </BrowserRouter>
    )
}

export default App
