import { useEffect, useState } from 'react'

import NavBar from './components/NavBar/NavBar'
import ComponenteContenedor from './components/container/ComponenteContenedor'


import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import ItemListContainer from './components/container/ItemListContainer/ItemListContainer';
import Titulo from './components/Titulo/Titulo';
import Formulario from './components/Formulario/Formulario';
import Input from './components/Formulario/Input';

 

// Name('fede') => <Name params='fede' />


function App() {
    
    const getFetchApi = async (url) => {
        try {
            const query = await fetch(url)
            const queryParse = await query.json()
    
            console.table(queryParse)
            
        } catch (error) {   
            console.log(error)            
        }
        // .then( resp => resp.json() )
        // .then( data => {
        //     console.table(data)
        //     // console.clear()
        // } )
        // .catch( err => console.log(err) )
    }
    
    useEffect(() => {
        const url = 'assets/DataPersonas.json'
        getFetchApi(url)    
    },[])
    

    const objStyle = { color: 'white',   backgroundColor: 'black', fontSize: 50 }
    // Titulo()
    return (
        <div>
            <NavBar />                  
            <ItemListContainer greeting={' Hola soy ItemListcontainer '} />           
            
        </div>
    )
}

export default App
